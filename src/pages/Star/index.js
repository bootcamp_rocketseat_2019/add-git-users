import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';

export default class Star extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('star').name,
  });

  static propTypes = {
    navigation: PropTypes.shape({
      getParam: PropTypes.func,
    }).isRequired,
  };

  state = {
    uri: '',
  };

  async componentDidMount() {
    const { navigation } = this.props;
    const star = navigation.getParam('star');
    this.setState({ uri: star.html_url });
    console.tron.log(star);
  }

  render() {
    const { uri } = this.state;
    return <WebView source={{ uri }} />;
  }
}
