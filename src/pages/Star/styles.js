import styled from 'styled-components/native';
import { ActivityIndicator } from 'react-native';

export const Loading = styled(ActivityIndicator)`
  margin-top: 50px;
`;
