import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DropdownAlert from 'react-native-dropdownalert';

import { Keyboard, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  Container,
  Form,
  Input,
  SubmitButton,
  List,
  User,
  Avatar,
  Name,
  Bio,
  ProfileButton,
  ProfileButtonText,
  RemoveButton,
  ContainerButtons,
} from './styles';
import api from '../../services/api';

export default class Main extends Component {
  static navigationOptions = {
    title: 'Usuários',
  };

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  state = {
    newUser: '',
    users: [],
    loading: false,
  };

  async componentDidMount() {
    const users = await AsyncStorage.getItem('users');
    if (users) {
      this.setState({ users: JSON.parse(users) });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { users } = this.state;
    if (prevState.users !== users) {
      AsyncStorage.setItem('users', JSON.stringify(users));
    }
  }

  handleAddUser = async () => {
    const { users, newUser } = this.state;

    if (users.find(usr => usr.login.toLowerCase() === newUser.toLowerCase())) {
      this.setState({ newUser: '' });
      this.dropDownAlertRef.alertWithType(
        'warn',
        'Alerta',
        'Usuário já adicionado!'
      );
    } else {
      this.setState({ loading: true });

      try {
        const response = await api.get(`/users/${newUser}`);

        const data = {
          name: response.data.name,
          login: response.data.login,
          bio: response.data.bio,
          avatar: response.data.avatar_url,
        };

        this.setState({ users: [...users, data], newUser: '', loading: false });
      } catch (err) {
        this.setState({ newUser: '', loading: false });
        this.dropDownAlertRef.alertWithType(
          'error',
          'Alerta',
          'Usuário inexistente!'
        );
      }
    }

    Keyboard.dismiss();
  };

  handleRemoveUser = user => {
    const { users } = this.state;

    this.setState({ loading: true });

    const newUsers = users.filter(usr => usr.login !== user.login);

    this.setState({ users: newUsers, newUser: '', loading: false });
  };

  handleNavigate = user => {
    const { navigation } = this.props;

    navigation.navigate('User', { user });
  };

  render() {
    const { users, newUser, loading } = this.state;
    return (
      <Container>
        <Form>
          <Input
            autoCorrect={false}
            autoCaptalize="none"
            placeholder="Adicionar usuário"
            value={newUser}
            onChangeText={text => this.setState({ newUser: text })}
            returnKeyType="send"
            onSubmitEditing={this.handleAddUser}
          />
          {newUser.length > 0 && (
            <SubmitButton loading={loading} onPress={this.handleAddUser}>
              {loading ? (
                <ActivityIndicator color="#FFF" />
              ) : (
                <Icon name="add" size={20} color="#FFF" />
              )}
            </SubmitButton>
          )}
        </Form>

        <List
          data={users}
          keyExtractor={user => user.login}
          renderItem={({ item }) => (
            <User>
              <Avatar source={{ uri: item.avatar }} />
              <Name>{item.name}</Name>
              <Bio>{item.bio}</Bio>

              <ContainerButtons>
                <ProfileButton onPress={() => this.handleNavigate(item)}>
                  <ProfileButtonText>Ver perfil</ProfileButtonText>
                </ProfileButton>
                <RemoveButton onPress={() => this.handleRemoveUser(item)}>
                  <ProfileButtonText>Remover</ProfileButtonText>
                </RemoveButton>
              </ContainerButtons>
            </User>
          )}
        />
        <DropdownAlert
          errorColor="#ff6666"
          warnColor="#ffad33"
          closeInterval={1000}
          ref={ref => (this.dropDownAlertRef = ref)}
        />
      </Container>
    );
  }
}
