import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Main from './pages/Main';
import User from './pages/User';
import Star from './pages/Star';

const Routes = createAppContainer(
  createStackNavigator(
    {
      Main,
      User,
      Star,
    },
    {
      headerLayoutPreset: 'center',
      headerBackTitleVisible: false,
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#4da6ff',
        },
        headerTintColor: '#FFF',
      },
    }
  )
);

export default Routes;
