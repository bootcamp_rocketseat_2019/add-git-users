# Detalhes do projeto

## Utiliza-se:

- React Native
- Axios para acessar a api do github
- AsyncStorage para armazenar os dados no celular do usuário do app
- Styled Components para gerenciamento de css
- Reactotron para auxiliar na depuração de código
- react-native-gesture-handler para melhor aproveitamento das características nativas de cada sistema

## Objetivo final

- Criação de duas telas:
    - Uma tela para adicionar usuários do github através do username.
    - Uma segunda tela para detalhar os projetos favoritos de cada usuário armazenado.